# Stream Deck Busylight integration

The `Stream Deck Busylight` plugin integrates the Stream Deck with the [kuando Busylight](https://busylight.com/) presence indicator. It's perfect for letting your family know when you're in a Zoom call.

`Stream Deck Busylight` requires Stream Deck 4.1 or later, as well as the official [kuando Busylight HTTP](https://www.plenom.com/download/177233/) software.

# Description

Currently, `Stream Deck Busylight` provides you with a single action which allows toggling the light between solid green and glowing red.

It requires no configuration as it connects to the kuando Busylight HTTP server through the default connection parameters.

## Features

- code written in Javascript
- cross-platform (macOS, Windows - not tested yet)
- localization support

## Installation

**Prerequisite**: Install the official [kuando Busylight HTTP](https://www.plenom.com/download/177233/) software.

Download the [latest release](https://gitlab.com/pedropombeiro/streamdeck-busylight/-/releases) and double-click the `.streamDeckPlugin` file, or [install](streamdeck://plugin/install/com.pedropombeiro.streamdeck-busylight) from the Elgato Store directly.

## Demo

[YouTube video](https://youtu.be/fgxbG2PBowo) - remember to turn on subtitles for description of what's going on. The Busylight action is located on the second row from the top, third column from the right.
